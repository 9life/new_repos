import java.util.ArrayList;
import java.util.Random;

public class HelperClass {

    public static int rnd(int min, int max) { //рандом из любого енума надо сделать
        max-=min;
        return (int) (Math.random() * ++max) + min;
    }

    public enum PriorityEnum { //приоритет вещей
        MostNecessary,
        Necessary,
        CanLose,
        ThrowOut;

        public static PriorityEnum getRandomType() {
            return PriorityEnum.values()[new Random().nextInt(PriorityEnum.values().length)];
        }
    }

    public enum Type {
        green, llong, wet, torn, pink, wood, heavy;

        public static Type getRandomType() {
            return Type.values()[new Random().nextInt(Type.values().length)];
        }
    }
    public static void print(ArrayList list) {

        for (int i=0; i < list.size(); i++)
        {
            System.out.println(list.get(i)+" , ");
        }
       // System.out.println("\n");
        System.out.println("количество элементов в списке: "+ list.size());

    }
    public static void print(Bag[] list) {
        System.out.println("список simple вещей");
        for (int i=0; i < list.length; i++)
        {
            System.out.println(list[i]+" , ");
        }
        System.out.println("\n");

    }
    public static void print(BigThingClass[] list) {

        for (int i=0; i < list.length; i++)
        {
            System.out.println(list[i]+" , ");
        }
       // System.out.println("\n");
        System.out.println("количество больших вещей: "+list.length);

    }

    // getRandomType() для любого енума
}

