public class BigThingClass extends ThingClass {
    HelperClass.PriorityEnum prior;

    BigThingClass(int a, HelperClass.PriorityEnum p) {
        super(a, p);
      //  System.out.println("создалась большая вещь");
    }

    public void raise() { // поднять
        System.out.println("raise");
    }

    public void put() { //положить
        System.out.println("put");
    }
}
