import java.util.ArrayList;

public class Relocation {
    public static void main(String[] args) {
        ArrayList<Bag> bagsList = new ArrayList<>();
        Bag bag = new Bag();
        ThingClass[] things;
        BigThingClass[] bigThings;
        CatsClass[] cats;
        ArrayList<TrunkClass> trunkList = new ArrayList<>();
        TrunkClass trunk = new TrunkClass(300);

        things = new ThingClass[HelperClass.rnd(15, 100)];
        bigThings = new BigThingClass[HelperClass.rnd(2, 10)];
        cats = new CatsClass[2];

        int subSum = 0;
        for (int i = 0; i < things.length; i++) {
            things[i] = new ThingClass(HelperClass.rnd(3, 25), HelperClass.PriorityEnum.getRandomType());
            subSum += things[i].weight;
/*
            System.out.println("вещь  ------- " + things[i].rndThing);
            System.out.println("вес  ------- " + things[i].weight);
            System.out.println("приоритет  ------- " + things[i].prior);
*/
        }
        System.out.println("кол-во вещей = " + things.length);
        System.out.println("сумма веса всех вещей = " + subSum);
        for (int j = 0; j < bigThings.length; j++) {
            bigThings[j] = new BigThingClass(HelperClass.rnd(100, 200), HelperClass.PriorityEnum.getRandomType());
        }
        // System.out.print("спибольшие вещи:");
        // HelperClass.print(bigThings);
        for (int i = 0; i < cats.length; i++) {
            cats[i] = new CatsClass();
        }

// !!!!!!!!! пока не закончились нагенеренные вещи, создаем мешок
        //проверяем мешок на вместимость в багажник, если ок
        // добавляем вещи в мешок, если переполнился
        // берем следующий мешок

        int i = 0;
        int j = 0;
        bagsList.add(j, bag);
        while (i < things.length) {
            if ((bag.sum + things[i].weight) > 50) {
                //   System.out.println("вес мешка " + j + " = " + bag.sum);
                bag = new Bag();
                bagsList.add(j, bag);
                j++;
            }
            bag.addToBag(things[i]);
            i++;
        }
        //   System.out.println("вес мешка " + (j + 1) + " = " + bag.sum);
        System.out.println("количество мешков = " + bagsList.size());

        run(bagsList, bigThings, trunkList);
//comment
// if (currentVolume < trunk.amount) {  && i <= bigThings.length || i <= bagsList.size()

        //------begin. рабочая версия складывания в багажник, но неправильная
        /*
                i = 0;
        j = 0;
        int currentVolume = 0;
        int countOfWalk = -1;
        //       trunkList.add(countOfWalk, trunk);

        //система вылетает как только в каком-то списке заканчиваются элементы
        //поменять на ду вайл

        while (((j <= bagsList.size() - 1) || (i <= bigThings.length - 1)) && ((((currentVolume + bagsList.get(j).sum) <= trunk.amount)) || ((currentVolume + bigThings[i].weight) <= trunk.amount))) {
            countOfWalk++;
            trunk = new TrunkClass(300);
            while (((i <= bigThings.length - 1)) && bigThings[i].weight <= (trunk.amount - currentVolume)) {
                System.out.println("вес большой вещи = " + bigThings[i].weight);
                trunk.addToTrunk(bigThings[i]);
                currentVolume += bigThings[i].weight;
                i++;
                System.out.println("текущая заполненность багажника === " + currentVolume);
            }
            while ((j <= bagsList.size() - 1) && (bagsList.get(j).sum <= (trunk.amount - currentVolume))) {
                trunk.addToTrunk(bagsList.get(j));
                currentVolume += bagsList.get(j).sum;
                System.out.println("вес сумки = " + bagsList.get(j).sum);
                System.out.println("текущая заполненность багажникка === " + currentVolume);
                j++;
            }
            trunkList.add(countOfWalk, trunk);
            System.out.println("кол-во ходок = = = " + countOfWalk);
            currentVolume = 0;
        }

        System.out.println("текущая заполненность багажникка === " + currentVolume);
        HelperClass.print(trunkList);
        //------end
         */
        /* мы генерим массив простых и больших вещей + 2 кота
        с рандомным весом
        складываем в мешок вещи нагенеренные
        мешки должны весить не больше половины багажника
         */
    }

    private static void run(ArrayList<Bag> bagsList, BigThingClass[] bigThings, ArrayList<TrunkClass> trunkList) {
        TrunkClass trunk;
        int i;
        int j;
        int currentVolume = 0;
        int countOfWalk = -1;
        //       trunkList.add(countOfWalk, trunk);
        /*
        система вылетает как только в каком-то списке заканчиваются элементы
        поменять на ду вайл
         */
        int countAllThings = bagsList.size() + bigThings.length;
        while (countAllThings>0) {
            countOfWalk++;
            trunk = new TrunkClass(300);

            for (i = 0; i <= bigThings.length - 1; i++) {
                while (bigThings[i].weight <= trunk.freeAmount()) {
                    System.out.println("вес большой вещи = " + bigThings[i].weight);
                    trunk.addToTrunk(bigThings[i]);
                    countAllThings--;
                    currentVolume += bigThings[i].weight;
                    System.out.println("текущая заполненность багажника === " + currentVolume);
                }
            }
            for (j = 0; j <= bagsList.size() - 1; j++) {
                while (bagsList.get(j).sum <= trunk.freeAmount()) {
                    trunk.addToTrunk(bagsList.get(j));
                    countAllThings--;
                    currentVolume += bagsList.get(j).sum;
                    System.out.println("вес сумки = " + bagsList.get(j).sum);
                    System.out.println("текущая заполненность багажникка === " + currentVolume);
                }
            }
            trunkList.add(countOfWalk, trunk);
            System.out.println("кол-во ходок = = = " + countOfWalk);
            currentVolume = 0;
        }
        System.out.println("текущая заполненность багажникка === " + currentVolume);
        HelperClass.print(trunkList);
    }
}

