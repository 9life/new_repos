import java.util.ArrayList;

import static jdk.nashorn.internal.objects.Global.print;

public class TrunkClass {
    int weight;
    int amount; //объем багажника
    //    Bag[] bag = new Bag[7];
    ArrayList<Bag> trunkListOfBag = new ArrayList<>();
    ArrayList<BigThingClass> trunkListOfBigThing = new ArrayList<>();
    Bag aItemBag;
    BigThingClass aItemBigThing;
    int sumWeightTrunk = 0;

    TrunkClass(int a) {
        this.amount = a;

    }

    public int getCurrentAmount() {
        return getCurrentAmount(false);
    }

    public int getCurrentAmount(boolean isPrint) {
        int currentAmount = 0;

        for (int i = 0; i < trunkListOfBigThing.size(); i++) {
            currentAmount += trunkListOfBigThing.get(i).weight;
        }
        for (int i = 0; i < trunkListOfBag.size(); i++) {
            currentAmount += trunkListOfBag.get(i).sum;
        }
        if (isPrint == true)
            System.out.println("общая загрузка = " + amount + ", текущая загрузка багажника = " + currentAmount);
        return currentAmount;
    }

    public int freeAmount() {
        return freeAmount(false);
    }

    public int freeAmount(boolean isPrint) {
        int fA = amount - getCurrentAmount();
        if (isPrint == true)
            System.out.println("осталось места в багажнике = " + fA);
        return fA;
    }

    public void addToTrunk(Bag itemBag) {
        trunkListOfBag.add(itemBag);
        System.out.println("в багажнике мешки: ");
        HelperClass.print(trunkListOfBag);
    }

    public void addToTrunk(BigThingClass itemBigThing) {
        trunkListOfBigThing.add(itemBigThing);
        System.out.println("в багажнике большие вещи: ");
        HelperClass.print(trunkListOfBigThing);
    }

    public void fit(int w) {//поместится в багажник?
        this.weight = w;
        if (this.weight > this.amount) {
            System.out.println("I'm too big");
        } else {
            System.out.println("I'm just the size");
            //метод добавления барахла в багажник
            //           addToTrunk();
        }
    }
 /*   public void addToTrunk(){

        for (int i=0;i<7;i++) {
            bag[i] =new Bag();
        }
    }
    */


}
